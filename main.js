class game {

    constructor() {
        //setting the html dom
        this.divSet = [
            { 'name': 1, 'nbr': 1 },
            { 'name': 2, 'nbr': 2 },
            { 'name': 3, 'nbr': 3 },
            { 'name': 4, 'nbr': 4 },
            { 'name': 5, 'nbr': 5 },
            { 'name': 6, 'nbr': 6 },
            { 'name': 7, 'nbr': 7 },
            { 'name': 8, 'nbr': 8 },
            { 'name': 9, 'nbr': 9 }
        ];
        //arrays to compare the winning combination
        this.dataX = [];
        this.dataO = [];
        this.winningPositions = [
            ['1', '2', '3'], ['1', '5', '9'], ['1', '4', '7'], ['2', '5', '8'], ['4', '5', '6'], ['7', '8', '9'], ['7', '5', '3'], ['3', '6', '9']
        ];
        
        this.squareSet = document.querySelector('#game');
        
        //create squares for each Set
        this.divSet.forEach(item => {
            let square = document.createElement('div');
            square.classList.add('Square');
            square.dataset.type = item.name;
            square.innerHTML = item.nbr;
            this.squareSet.appendChild(square);

        });
    }

    Player() {
        this.symbol = document.querySelector('#player');
        this.symbol.innerHTML = "X";

    }

    start() {
        this.squareSet.addEventListener('click', event => {
            let clicked = event.target;
            
            //check if the square is empty by checking the dom class
            if (clicked.classList.contains("Square")) {
                //check who's playing by checking the innerHTML symbol
                if (this.symbol.innerHTML === "X") {
                    //replace class, innerHTML, change symbol and push into the comparaison array the data-type of the clicked Dom element
                    this.symbol.innerHTML = "O";
                    clicked.innerHTML = "X"
                    clicked.classList.replace('Square', 'SquareX');
                    this.dataX.push(clicked.dataset.type);
                    //check if the clicked element trigger a win or a draw
                    this.Win();
                    return;
                }
                
                //do the same for O
                if (this.symbol.innerHTML === "O") {
                    this.symbol.innerHTML = "X";
                    clicked.innerHTML = "O"
                    clicked.classList.replace('Square', 'SquareO');
                    this.dataO.push(clicked.dataset.type);
                    this.Win();
                    return;
                }
            }
            
        });
        
    }

    Win() {
        //use array.filter() to parse dataX and check if an array.length of 3 includes any of the array of winningPosition
        var combinationsX = this.winningPositions.filter((combination) =>
            combination.filter(x => this.dataX.includes(x)).length === 3);
        //do the same for dataO
        var combinationsO = this.winningPositions.filter((combination) =>
            combination.filter(x => this.dataO.includes(x)).length === 3);
        
        //the length of combinationsX will be 1 if a winningPosition is detected 
        if (combinationsX.length === 1) {
            alert('Player X win');
            location.reload();
            return;
        }
        //same with combinationO
        if (combinationsO.length === 1) {
            alert('Player O win');
            location.reload();
            return;
        }
        
        //if all Square class are replaced, then it s a draw
        let selected = document.querySelectorAll('.Square');
        if (selected.length === 0){
            alert("draw !");
            location.reload();
            return
        }
    }
}

let letsPlay = new game;
letsPlay.start();
letsPlay.Player();
